# Md Sabber Ahamed and Eunseo Choi
# Center for Erathquake Research and Information(CERI)
# The University of Memphis
# Benchmark-1 of Energy Balance equation. Details of theory given in the paper
# In this Benchmark we included only plastic power term in the energy
# balance equation.
######################################################################################

import sys
import numpy as np
import matplotlib.pyplot as plt
from math import pi, sin, sqrt

sys.path.append('..')
from Dynearthsol import Dynearthsol

def save_data(file_name, data):
    file = open(file_name, 'w+')
    np.savetxt(file, data, fmt='%1.4e')
    file.close()

# Vector initializing

total_length = 2001
stress_xx    = np.zeros(total_length)
stress_zz    = np.zeros(total_length)
pressure_ana = np.zeros(total_length)
temp_ana     = np.zeros(total_length)
#ep_xx        = np.zeros(total_length)
#ep_zz        = np.zeros(total_length)


# Parameters
mu    = 200e6 # Shear modulus
K     = 200e6 # Bulk Modulus
vx    = -1e-5 #Velocity applied
c_p   = 1000 # Specific heat
alpha = 3e-5 # Thermal expansion coefficient
phi   = 30*pi/180
psi   = 10*pi/180
coh   = 1e6 # Cohesion

e1    = K+4.0*mu/3.0
e2    = K-2.0*mu/3.0
sf    = sin(phi)
nf    = (1.0+sf)/(1.0-sf)
sp    = sin(psi)
np1   = (1.0+sp)/(1.0-sp)

rho   = 1.0 # Density
c_p   = 1000 # Specific heat capacity
alpha = 3.0e-5
temp_ana[0] = 273


# Calculation:
displacement = abs(vx) * np.array(range(total_length), dtype=float)
for i in range(1, total_length):
    de          = (vx) / (1 - displacement[i]);
    yield_time  = (2.0 * coh * sqrt(nf)) / ((e1 - e2 * nf) * abs(vx));
    beta        = ((e1 - e2 * nf) * de) / ((1 * (e1 + e2) * nf * np1) - (2.0 * e2 * (nf + np1)) + (2.0 * e1));

    if  (i < yield_time):
        #Elastic stress:
        stress_xx[i]  = stress_xx[i-1] + (e1 * de)
        stress_zz[i]  = stress_zz[i-1] + (e2 * de)
        plastic_power = 0
    else:
        #Plastic stress:
        stress_xx[i]  = stress_xx[i-1] + ((de * e1) - (2 * e1 * beta) + (2 * e2 * np1 * beta))
        stress_zz[i]  = stress_zz[i-1] + ((e1 * np1 * beta) + (de * e2) + (e2 * beta * (np1 - 2)))

        #Plastic strain:
        ep_xx         = (2 * beta)
        ep_zz         = (-beta * np1)
        plastic_power = (stress_xx[i] * ep_xx) + (stress_zz[i] * ep_zz) + (stress_zz[i] * ep_zz)


    #Pressure:
    pressure_ana[i] =-(stress_xx[i]+stress_zz[i]+stress_zz[i])/3.0

    #Temperature:
    tmass         = (rho * c_p)
    plastic_power = plastic_power/tmass
    temp_ana[i]   = temp_ana[i-1] + plastic_power




def numerical():
    des    = Dynearthsol('result')
    dis_n  = np.zeros(51, dtype=float)
    Sxx_n  = np.zeros(51, dtype=float)
    Szz_n  = np.zeros(51, dtype=float)
    temp_n = np.zeros(51, dtype=float)

    for i in range(51):
        des.read_header(i)
        s = des.read_field(i, 'stress')
        T = des.read_field(i, 'temperature')
        x = des.read_field(i, 'coordinate')
        dis_n[i] = 1 - x[3,0]

        #print abs(s[1,1]) and temp
        Sxx_n[i]  = abs(s[1,0])
        Szz_n[i]  = abs(s[1,1])
        temp_n[i] = abs(T[1])

    return dis_n, Sxx_n, Szz_n, temp_n


displacement_nn, Sxx_nn, Szz_nn, temp_n = numerical()

file_name = 'benchmark1.dat'
data = np.array([displacement_nn, temp_n, Sxx_nn, Szz_nn])
data = data.T
save_data(file_name, data)


#Ploting results :
fignum=1
plt.close(fignum)
plt.figure(fignum)
plt.plot(displacement*1e3, temp_ana,'k-', displacement_nn[::3]*1e3, temp_n[::3],'ko')
plt.xlabel('Displacement (meter $10^{-3}$)')
plt.ylabel('Temperature (K)')
plt.legend(['Analytical', 'DES3D'], loc='lower right')
plt.show()
