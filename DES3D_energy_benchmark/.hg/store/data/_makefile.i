        H  &       ��������p�Y�n#tUo�L	��a���{            x��T�o�8~�Ũp=8JZ�[��JI��-P]O:ie�Xul�q.���;vh���ey �_�|3�qF��+}a	�
�K�E�4��6�J������I��!LaM%|2TNn���r.yD=�)oЛt����0N2�=� ���N���Wt!=X��b�)���G�	[�o��b�<�k���2�H��
&>L곳� ���A����,�M�E|c������}�c�b�Ef�!g;l�&��g�o�Cb]~��M�6P������������
QA�
(��݀dX�Q���'FU&uu1����ch��<�ޯ��і̃� 2Bx¾C��w���ŀt�CtZL9����џXa��~�Z���7�ĕtS����^���xl���9N�ɽP�QM3f��eBw_,�^��6��SB�����Mo4՜儬����f�L��4�q��-R��?����}0�δg�B�a�^>�iN�V����E��"�dN����@!z��uM݆�}���G}��B6~Є�g�%�^o��͍u����-��v!�t��ʀJ���-�[c�hv2���'�nN�m��(�t���1��y��9��pz
}��E�Q]]Y��d�Nm�*l=���y
쿽��v�i�O�_ě��6�큫f��"*%�a��Pr���>�f1�Q�`��t�
�0x�������oA8�O���b��I5M$?O���c��;Н9n��_<X�����Ag�-+DT��ʱƾ�b��}��'���໌W�K��3X3s�$>�`?\t��͡vL��;ލ y�;a��H�;�=l��B�&x»������3E�Q�#k��(���o������    H    9             �����8��4�c�݄;���a            x�m��N�@�Ƕ���'�XL�(�m��5U��QJIۭ����^��|�|=9�"��$?���|;� � 綠Vg�l�c��L�(=F�BS�`Gj��YU*�S%��"_2��E�ó� ;*:e���N���_�Nc1_fb���Hz�⩽�>T\�T��l�>�����MΚWq��$X�CP$�;��peR��}�ve�Y:�ӿ7܆!w�\�iV+ВN��]�a)�:f�\����7��F.��Yq8E%B�	����oy�2ܼP��@�K4�H_t@�l�p�H�|'����Wg��[!׆��v�~ �^~�    �       +          �����t*�%��C$+l��}%�              �  �   	parameters.hpp
    �     L  .          ������1�9���3'w�,�D+                   
ndims = 2
  \  �   *BOOSTLDFLAGS = -lboost_program_options-mt
    �     H  K          ����kUqw/���o5���,�G�^            x�c``z�������A�3�2/5��$�8?�8E/��B!��37�8��b``f�`A�����ļ�b����2 ���    1     >  l          ������A4q��'��� �.ד��              �  �   2	CXXFLAGS += -O2 -DBOOST_DISABLE_ASSERTS -DNDEBUG
    o     L  �          ������U��3���'�s}sF3�              s  �   CXXFLAGS = -g -std=c++0x
    $   	mesh.cxx \
	sortindex.cxx
    �       �          ����x�4/l21�mۧ�!R��              %  %   	matprops.cxx \
    �       �       "   ������-Yy�檻���Ԫ}g&J              %  %   	input.cxx \
    �     �  �       0   ������8 %4:��&O��F�Tc             x�c`````Ve``�L��KM,*�(���K��P��Ju������W�����Fp�s�H����F^Jfn�f�^�&H�B �
T,��
T�d \%P�W �E�h�LմRPE6J�9@E���9X��SE�9"BSL��8���N���!�d�|. �,2.    �     )  �       :   	�����J�Ls��CN6�ա�E�1�              v  �   	parameters.hpp \
	utils.hpp
    �     v  	       ?   
����j�zZ�m�Kþ��ׇI�            x�c`�H``������é���� ��|݃5t]�\\�B݁��O� ǠH��|�Ҽ���ݤ����ݲĢ�Ĥ�T�df^�nI�nA>��Z���X\�����b������� ?_!�    2     8  	       M   �����u��e�B��1.�a�Y              �  �   ,	CXXFLAGS += -O0 -Wall -Wno-unused-function
    j       	(       T   ����� MR������ɷr�6C��              8  8   	geometry.cxx \
    �       	6       [   ����}�>�ơE�O�=]��d�K              q  q   	output.cxx \
    �       	F       c   �����6�#�z}�L����߈rt���              �  �   	matprops.hpp \
    �       	V       e   �����E7:h�{��"3���r��7                   	rheology.cxx \
    �     �  	�       r   ����'�����ޮ����Vl3e"            x�c``�� ������.�A
�
�ƺ�\`O'?G_W� P�y6��i*���V
*���N^��@��������B���K��
EDhb�������f:������>H�*�������� t!�3Xr��Fr
�s�r�S5�� E�?$    u    >  )       �   ������8�[�]�v�yEd]?j�\
,            x�e��N�0ƫ^�w^xBL���饑Dp���3"wn�h�d[�Y���=<㏡�i�~��z�WBN�TA.L*�2�.84NWFV'BN3�/L�SR|@��0��;�v�Zx~��Y,ˣnh��'���NH�%��kX[P$ދQ��$+�`n���8p�x�?���������'j�$G���Y��+t����B��2Kt�$��63c�G-���N���L��t�3+�6H��r�C"��0tꗐ�st��Z�"�hL�B����,S��U¹��DB*��9?��O6A�J�(&⪁$9+}��q��    	�     4  7       �   ����s�����g_*��E�>����              �  �   (	CXXFLAGS = -g -std=c++0x -march=native
    	�     c  y       �   ����G����VJ���,v]F^�n            x�c`�fd c�Ȕ�Ԃ���<+.N��\�4�אx'�`M;�ΆѮ��\��)@��q�q��g���Bnbv��0M..�@l�Da2�	\ aQ    
J     u  �       �   ��������JW�յJ�j�l�8            x�c``�b``�f``(��TV�w��tr���pr��q�Q(���QH�KL�IUp�U��L*.I��V(H,J��I�Q��OI����r�qtVжU�M�/H��-P�u	v��� �0�� HP#8    
�     C  y       �   ����`i���i�ݪp0�_iV              +  5   
gprof = 1
  j  �   !		CXXFLAGS += -fopenmp -DUSE_OMP
         ?  k       �   �����>
 3bQb�X祈�M��O            x�c``�f``4e``�J/(�OS�U0�b``��ʜ��n>���
ڶ
��\��>.( ��    A      n       �   ����C�F(|��Ct���j��0            x��R�n�@v�sD��i�ph�:J�[����)�nE������WY���$�}ބO��#���($�ޝ�����y^�y�'�U��M���.Ɇ��e�&r��Lmb����C����	xRQV��T�ҵ�sW�r}������1�B�-��܃��#MamO��4Uz�[�:�ׅ�Վ�
��"t-߸`����([RaH��1��_:)�T!��'h=���!u��c`��g����b���gǘ�=�a�o�^�V��y��h�tE����u<�,fW�8�����>�M.�~�r��:�v���Ƽ��J9�B订��b�	XL"��*�[�%Lo`t@
9i��I�7�AP���A�P�i90�JdY�Ӎk�LAƍ�K[F���<��BI�.e��-$��;W���z1��\�P;�-Q��?��#��)d`�NL��?��#@���������&|E��1�Ep�@
��-ك��2]qwE?U
��Z��-,q���@��� �~�6    C     &  `       �   ����%ꪁ�EQA�+��5��\��              �  "   	CXXFLAGS = -g -std=c++0x
    i       o       �   ����6<��J
�{d����g� C�              �  �   	array2d.hpp \
    �     7  o       �   ����j�.	��������y2Y�q�              �  �   	rheology.cxx
       	sortindex.hpp \
    �              �   ���������l�	5�[g�����            x�c``��������!FYY�)?��D!'?9�$3?O!1/E!'3�(��R!/17����?8$>��?$��3H�*�������`����2"��(?�(17>� dP1�sf��������j'��7�SEU��~f^rNiJ*L5�^m�jL�@�+�����$�d�bU�������t!g* ����!�
*���N^� ���	b�8B�g�c������|<��}]�"�
*@{���x2�#�=0#�E��&+��@u 
s    �     b  �       �   �����d�#�WP����|�x٠            x�c``~���"��� ���������m���o����$l�F ���@Y+tٴ��ݴҼ���ݜ���b �<#?'U��(?�(1��)�� ���    $     @  �       �   �����%'N�h�����e}Ot���            x�c`��a`�ua``��t�V��u�v�T�uVP�p6�0�w������@U
8U)$�&�q 
e    d       �       �   ����c�0�#��[�On�ui+�              �  �   
	ic.cxx \
    z       �       �   ����C�|�(�h�� p1���Ԡp              �  �   
	bc.cxx \
    �       �       �    �����L$��f�:>z�P�S���              �  �   	fields.cxx \
    �       �       �   !����{_Yv���+$�6���ו                   	remeshing.cxx \
    �     �  �         "����E���yr4f�j�rGV            x��O;
�@�v��)����.�Ǩј���B�B���[y'e�b�ߛ��0���{I�O�5:X�54]���
iBJMco��堌�ǋQY���@L��%��a�*6�*O�E&�z�`���iU�����5j�w�A�ӕR�'{<im6����&�����5}�՗<6{]����(�h�������7lw-2�-�@{���\�ɲ ^��`    �     $  �         #�����ҫ�� G7�뇊�#b�K޵                   	nn-interpolation.cxx \
    �     3           $�����H�W~n��B�}T8���j�            x�c``�� �b�I�E�ɩy%E�ɺiyz�
1\�)��"�� � (��    �     %  $         %�����|�ĸ��Z�T���H�V��              �  �   	brc-interpolation.cxx \
         O  R      �   &����<8��懭K	�`�c���AY�            x�c``�a``�b``��,�H���O��K��P����M,�N-*N-	p���@���%�9�z`u��p6\P� �fI    m     d  N      �   '����g�����y��7��tl��            x�c`�c`����0�7>8�9X�VAE#$�������Cq@��N^AG��+3-�PACE#/%3�XSG�X��j�6X�k�PN��pa���Ps�����'�    �     o  �      �   (����+��a����~�!k��\�v�            x�c``|���$� �P̠����������`������_\_P��^���_P���W�T��������|T�&���Ļxi��$��*�����$�d��P� ��'�    @     n  �      �   )����}� ��E�[�Wj�zUK            x�c``�d``]�������������m���o�����$��uK�J�SSt��2�rRQ�J�K2��`��y��y�E�鹉�
�iI��y)ź����@nIQbA 8&�    �     N  �      �   *��������4`�ƄQD�En<)�              �  �   Bifneq (, $(findstring g++, $(CXX))) # if using any version of g++
    �     U  �      �   +��������{*���c����,�            x�c``vf``���������?8$������=XA�VA�GE"��������������[T�X�a�UqF�$��6�� �D1    Q        �      �   ,�����WBФ�8Q���1�V$�              �  �   	phasechanges.cxx \
    q     )  
      �   -�����4FY�?�:1�S,��%c�;              �  �   	markerset.hpp \
	output.hpp
    �     _  �      �   .����L��J3�V�����¥���I�              �  ~   S		CXXFLAGS += -march=native -O3 -ffast-math -funroll-loops
	else # debugging flags
    �     $  �      �   /����}R��8۾�e�B�{�)�".              6  l      �  �      �  ?             -        �   0�����)���¦Ȏۧ�D�KaU            x�c``b cΤ̼Ģ��|��
�.vF��(( � .QL    J     P  e         1����fFNwHΣ�5oe�ou� �            x�c``�e c��̴�B���Լ�MCM.NN�7G�`m[�4����Kh�k��o P���<gj^Jf +��    �    �  ?         2�����h+�缤5�;�[�O�)�            x��SQO�0�x@���O8A�&i�A��1F�І��T��R[8v�$$~ /�������i�4( ��s���;����ك���yJ{Pa#�F�������|8M������x�ƀ�A�]c㵳�]���n�׻�5&�1~KvG�{;'ß?�r�%
A\b歨�r�sr�=�O �>깱��G�7����?|A�|7b��X@p ]Uk��1$����S�䋴.�w<�uSpu ��袭�UtR�Jr2�5��8�z�mz|��tpz6]d=���@�� �$�~���sb'�G�ڪ��r�s�@wU=�E����93�G������u�By�E�m
/�D�=
������h���q�n�"HY�E#ʐ�����x|�b�U�I�'�*$Hj��q/��,�+�,M�/�@@    @     �  H      �   3����Kb��;đ��o���	N            x�c`��f`�Mg``��tHM��W�MUP�ɋɳ�����ĢJ��4���T����T������Bq^bAqF~�^JfZИV >4��(g���|����Ē��<�����������<M\�>g`��J���JK@��&�d&�  k�L    �     �  b      �   3����J����O����=ET�            x���A
�@�5�E'�!AE��EDԢ�A��S:5.�A��ݡ��"z����G���G"9�ॆ�W{5��]S��r�.`J�s�v�a2�U���f�ˢ�X�n����Aڞ���$gFj_i^
~y$U�_䓨7���?��5�s�U�����;4p�����j J���fs�-�.�U�Lu^�hS�    �     M  t      �   5�����b,��"֩!�Q���1g�3�              �  �   AHAS_HG := $(shell hg log -r tip --template '{node}' 2>/dev/null)
    �     �  }      �   4   6E�0�?ɝ"����1H����            x���1�0P� �3��0�� u��uI�D� qE��'�v܃�db@������5Q1�auT�0*�����������A!J4&/��p�V�ub�i_�:�Ph�㔴R�[��(ySCe�K�hsN��/�+���d���ʅ��L}    }     �  �      �   7�����bTR��>R���Yb5*Pq            x�c`�e`�a``��tHM��W��S(�K,(��/�K�LK�" �n
�����E�
�i
%�
��)�
 	uL}@�f�����۩��"7(���X�������_�����������������|!@��k=�j����k
J�r2�q� �u�         �  	      �   8����n�� �BP�}Y���]�~x            x�c``��������A���?8$>��?$��3H�VA?��D?'?91G?)?��$�0��<ހ����*�R����]�n>���@]��*�Firq*CD|\ ����|0���d&)����$�d�bU��I�Q�%��h��Q� #J)    �     �  �         8����Y,Վ�'��TW�
�dd6���            x�c``����x���A���?8$>��?$��3H������-�6��N��sD����{��������M�̼�ҔT.�j�bm�jL�9�I
��9:�E�%�XiTqIbz*a�ʀ~�{������	�CF�BN~���.P_r��n��R^~�BAiRNf�������Bq^bAqF~�^JfZ $V�    X     �           9   :tz����p��	*a�����I            x�c``��������A���?8$>��?$��3H�VA?��D?'?91G?)?��$�0��<ހ����*�R����]�n>���@]��*�Firq*CD|\ ����|0���d&)����$�d�bU��I�Q�%��h��Q� #J)    �     �           :�����]���m�X���:	����            x�c``����x���A���?8$>��?$��3H������-�6��N��sD����{��������M�̼�ҔT.�j�bm�jL�9�I
��9:�E�%�XiTqIbz*a�ʀ~cO�`��dݢ��ݒ����
�. �L�    y     r  �         <�����w�:b��Z�2�`27X��            x�c`�cg c�Ŝ���
궶
��)�
I�ũ)
�y
9��%
�I9��
E�e�ř�y�
���
vv
�y���%z)�ii\��
9��
�E
J }}��J�T��$Z :�5x    �    @  b         =����pѹ-~6$w��{M)O�B            x��R�J�0�7{����ئWB/ĩ7�	C؝��KӘ�n������t��M�����|�v:矝�=�@����
��!5w�J��d�E�f�L�zA���Q+#�<:�6ү�m|م�	λ���2 9�yƆ�Ѷ̦-��nK����I
�%�
�s>n���}��{��"��[�V�=Ui��^Vʍ+��N�"��/�j�P��!E��@#���Ǘ*��؅��;��[![g��Z��DLwf]f7��T�*�ϨRu��Аp��Oͥ�mE��-����iq�<x<��{��ۿ 撷T    +     �  s         ;   >n%dx$h%ء"�E>���v            x�c``��������A���?8$>��?$��3H�VA?��D?'?91G?)?��$�0��<ހ����*�R����]�n>���@]��*�Firq*CD|\ ����|0���d&)����$�d�bU��I�Q�%��h��Q� #J)    �     �  v         ?����JJ+�t�2��b�k�����	�            x�-�?KA�_
A�&
V6�ؘb߻cl$h���r�6{�����
I�w� ~"�~0� �� � .���Ap�q����Vi�ɷY����u������"=��='4{��kc�1x�×���h�r�k�����~ �?�O�M`�-��Օ�P�<����d�pe���[�_�45�]�J���A���t��\.�o���Tت�~��8U    �       v         @����h�\��ƭ.<��U��i.@-�              I  T   openmp =0 
    �    �  �      "   >����I��M�冤0���� �QV4            x��T]O�@Ш���D�o��m�x2)����0�`��N��ٙ23e��_���?�/�z�.˲�q��4����9�c���CB�!Ou�U/�Z>����
�/޵ۧg��v��spx!:�!����e�] �bt�E)߀XΜ6׾�E�SY;hD��F_ȘE&�ϻ�F|O$�MD�	Ѕ�J�<%M&T
.� B����j\�W����?��x������,b>���5�Må��"���� ����J_�l~������+�n�\�#�W�Hp��k�g��D��**����`9+M(6XF��(�%0�˅䘼�8=q]��֎�g<��uq���6#�M��=d�Gg��s4��S��i�F�a6��[���]��97��NcK�!rp����{��$�u��:)˰�~�9��Z��	9��n��e��vN6�@�#J(�4�eόM�D�H���UjPaN�4ܪ�FgF��k�]J�=�|���>�4��!z���ǿp�ƛ��'5<���|����.�E~x{�eV����Ѝ,�(�$Y6#/�R00�JX�_�U�����fڭ#a�{{Y
R��X*�u\#�f�+�{�	y~���ݹe�x�YV1*}#�1��/��%    !<    P  �      (   A   BF���y�$�Y�5\�ɣ            x��P�J1ME����.݄ꢥM2#ֺp|�Ph�4��L�0���Op��_�Wܺ��^�����p�rs�='A��B��C�
'c�=��z>C�������������`3�4K��)H��������L��D0�Tn�̥�}�H����iB]�G�L��XN�mP�ac������m��~��}rхT��]��b[�H��QUA��!ci2��2��Lq{�
��Y�k�k�l� ���b�V��q����B�#��XA���H�B>$:+*�S|#ko�ߛ��+��:�#���D�I�KÌ�t�R���O��c��0��O=֍,    "�    �  �      *   3����׍�K�����7�m`���ʶ            x���݊�@ǧ
B���^
,lKw��Y?�m�~h����V�E!�ɴ	&3�||�/|)oć��W�L"QV�������d�='��"�<���Ma�{wƃ0{D��q@���z�g�v�!�輐�/��9�dw8�/W�b>_���u�ۘE��R3�"Z�
'����)�w_r�1�~�b�l�FL�Ƞ�F�
n����aJ�H�5m,��.�k�T��}��(���2�W�aўz=X��xكF3v�J�y��@o�I_u�A��d�7��$[G"�q6�h�b8A�לb�sN�"�l����1٨T���.��t�gR�-��`���+� ��$�-M��1>���^�#ik-C������F:0��c&�E�U��8C����~��>���_1�p�����4���$.[8d��}��ƮH:���!�x��9�*F�,(�l+���E����̡o�m�x�]D.�����m����[߳o��7�^�c��    $�    -  }      -   D����H�j}a�^��	4�	Z�             x�c``�a``�e``t��P�UH����-�5�3�J,�@I'���� ��x� �:���Ԣb��Ҽ��|��������x�xS�x�^���@��
P 1h����{0�]OT�5����+�FE__wc�xO?g�Z�H�8��(9����
�j���C�=��+N�/IΈ7�3�3���x:A$U4j5�s2��T�\��g�!�S+2K���Ԓ�b.�Cxā�萙���>*���`�J�<��~���@�����~�� \E�$?��A�P�B�� Ԩ|. �}    %�     �  6      3   E   8)J�1�����pޕ��"�{            x����J�@��Th���`%	���*V�=س�����Fї�|_�<Hā����~�� �Q�(֛�Χ'���@�`�Y�kd��Um������%�Y>�ab;c҈�@��cj8�\9�9Z��V9*tYF���4�]U��	��W<���{��A4
�ڿ31�����kg�X�����Rdڦ؃=������v}�����&9��g'�dY�\�����j����M�Hf���F��:�R    &�    �  �   G  6   F������w(��VO��E>�	(             x��Xmo��l��A�"V�I|m�e���KrM���{@ C�hK�DjE*�����3%�/�g193|8�����ՅG��/�ӄ��a!s����u4�Iwh��BG2��(��/�틫Ϝd<�8�=�^}Biv|���Q��G/ �XB�˰t,��`��2 3}}���H��#�G�_�咖�,���T�5t{�B C�3\���B!��K28���?<��"NA��� 7�r!�Nk{�����#��c���D�bas�)Ox�apv�z��2g��_��<;s��������RiHd�+�"�$~��|�O9�����d<�͇�\�yV�p�Bq���;�1����r~A�Χq��V;��E�/ɴ�U�'�ֲ���G�j	���W�ud,��r�<�q��>�o�h7)m�n���tN�-�b�"��6%�)�x95��I{[���v<C�=����w���wr�景���1���`���ߠ}'�E,B�s�:QPA�q��^O���v�w��� 9�jc	���upvv��ZM+�����u�F9�p��f1�k|�Z<Q����
vIW���<N��~r�js�59Jr�O>Vo��L�<��>�w�H�.����4�u��B�2I�D�Lٝ�Ϸt�]/�*�K�n!P��}��ؼ�&��xd�o5u��_����_e�7��p�s?{G-�l���A�.l�p����|�Ѥ�j���טt@��@
|f+Mo+�U���<�1<�\)t(��&|{����=��̃H��sy��I�Cy��7��[[w:E�O	�X�w�H�T��a�W(+�Q�<(Ш	�<�*�2_������[�]����-D�n��7���	�{GI�$c�� ]���Z�
t�<܅��� j���<�db"g��Q,pi,�y�&E)�T�E̓PU�%�)�����5T,�BWtC�/Y��Uc!k%�@�"_�u�b�k��:pM��L�r��6Ǹ��Aa���2�����*��,�}{U��v`XĬ�UE�|�U�)�9�\�\cT���q��&+�++U-�g��o~� tҦ+��a����I�N�I����
4��f3�o&?�\16�楫��z�E'�y��������X�����aF�U�WA[I�����\,ީ^`��;2���B���B��$Z��H"n��ġ�QF��R~Y�b�5�c��&��-�[�K���5��MF�a��ݯ][�t?��$<��<�G%���ӓ��0�� ����z��ӉE�Xñ�����Ђ�����Ӆw�N1����;��e���j1̽f���Ю ��i����
כ�5L���Ϊ���h�wT�[q�d��e��6&I�����ĺ��Í�y}���܍�~�� �X�k�~�"���
��5g�UR�[����껺q���Z9���C~)Ӹ�R�6��23��VY�T��Jn�břUc��h�M���Ј4�Q�]��m�i��.Ձ&�BI8��ؖ�)C5�\K)������*��M�DUJ�����J)Sb�<^#��S���]T�N��Ь�ָ�a��Z�f�e|�!ȗ�����D��&���s�j�r �V�'��npΝ�E��k���HSj���#��!}��PV��ږ�0  ӶĶ!k�"S#aq��	���6�x��=�Y����6�� k(�,(D`���B���}qS@Z-^N��Vu������8'/-mY#�+C�}�=�K3{�)mV��r57/wCq8��[i�5B��[)����f�J�W^=�V1u
Ԙ�;Jd�{�8���σ�G����|8<�'��x2�Vm�Ɛ��[����Y���4� *�`����Eb��
_�q�2��kz�� /V?`�q<���X�s�q}����=���i|�y��[c�z֖�>���m��9즞�>�������۝�-:����N?�u����ISP�v��~��dDa��v�_���E��(Q�/�V�O ŇK�69�L~%�<���Y=5K�F���}��|����r�?T��h    .�     �  �   G  ;   G   B(`˝�wk�V�����,�~            x���MjA��M�Y�+��0��n��\���Iw�ݤ��x
��	r��$�:�@
^-���G4Z�Z"*��>�Ʋ zВ��v��^�<טΒa�`�pq��lTUf?�.3�s��/%��B��"��K��M�%ef���D4R�db~Ҷ�?xY�`w��;�{d�P���P��#��n���������Q�U]�1`b�2��YW6���M�)z�}cc�    /~     �  s   G  <   H������7�.�coC,�z�0�7e�            x�c``4g``td``��K��-V�U0�r�30p�����trtq����w������LJLI,(�,�,��BU���T����SS?3/9�4%M��K�L�~���@Hp��p�qtVжU��D7	� M. \?�    0         G  =   I����� ������'�;4���            x��T�n�@��%-$N�V"[�6!���rp��5�)"Q�H�f�NV]�[{�������=��	$��=`yvgvf��xf�Ў@h�B�Ys8�IN�O�e+�Vͮz��X�y�l��%#�q���Ǔ��>�4���x�i���Ob�� �sBO�Z)#��X$m{���g�����{��������ݖ�����G��6M8v��P"S1�3W��k��c�u������Qp�z�kyL�D�j�ƹ�,bP9�C���;�EBL�)[{�ۆ ��7�A>^���\]��A$�����`����|?N��r�m��,���Pl�ĥ������h9�L��g�@�"�T7�(S-��1:��;�TM(����,����E��
Hz���+fѮ�;н���_�d&�J���bF���������~X�Wj�;B{'p�����m�����g�o{&p�YVW���+&�랶M��p�����F�l��=G���7�?���B�.�̨��r&�    2      �   G  I   J�����m��A��z�������R            x��Sێ�05��������!QIBU.�J�H��d�hUEZ���k�s!IW�?�g��?��0v��K�N��=�3��3S��B��y���9%!I�ÝS�aΖ2��X���G4��	D"��,�q��X|�3*�`��Wz�M)8�78���uM� �3�1T�l� h������u�mF���z�R����*�C�J�B?��#�c����;w�������y�>ȝ�H�R��_�t?�cpSz�\!c[�����p��Mw�4���6O�+)�]qk��:ϱ9x�̇�L�u*w��J��)��[�R�aE{�"c��b��Ö��#i<!��$�������~�@��%�5T<c�w�qNR��f�I�ĝ��(#�r�����Љ���	ď��I�<!��)�/����5d^����L�u<����t:��:p�*�b�!�����ҍ����3��3$!"P#$MC����)�7�>\��F��q˞
(�-�"�P��[EO�S�%XE�    4!     e  �   G  M   K����A)I��i��ꖍ��<�                z   Y	CXXFLAGS += -I$(LIBADAPTIVITY_INC) -I/opt/local/include/vtk-5.10 -DADAPT -DHAVE_VTK=1 \
    4�    �  �   G  P   L����?ɛl`	ZA����7��<>��            x��P�N�@���@�p���#	7,xӈ�@C01N����#�@P�ʏ�c�x�B�h"@��=�̽gfB9��#B袭���u� ]m,5���P��� ��X�*�W�e��sX1L���5�n;Џ��sp,�|��X:�Or]Q�*f�ŗ��U
��l���Ҕd�Xi�V�ughS͸OL˩�9H7݆:lM{�fUz��#t����G���I��/��93�MC�VL<+T�d�iH�' &�1���gu��e�F�3.�����:됊��m�:��>`��/n�P�+�U�Hp����o˒�e��²��J%jM�ۥB��_�qM�)����|Э֒�dl�o������'/�A$"�΂�;<K�CCj}�c�BF�F��h    6
    �  �   G  S   C   Mp�a~V���X9�)BO�}            x����J�@Ɨ*H=y��V�F�)EmQ
�B[�!���Y�fc�����7��#�
>��F����``/���fv�	c�2c�.c���lw�V���j���X;Rq_:=�R�x�gޱi2�����.e���A�X�v��mJ����Zi�5juDXc	s��}#-;�}�+�M�R��Ⴂ�f�0��=�X�#������Vɣi�~�M�����HȀ�I ?�� D�h1�^�+���Τ�^�t� "B���P��v�MpD��0��/N�Lq��H�p�zC�ӟ[ߴ�U��h��p�@�'��B�~�}����[� .��n	�{�{2Α�
*H#?NC��$�7���U �ϼ,N�[D4"Mˠ!���%p�²��y    7�    g     G  T   N�����E:u̲w���etF&�h��[            x�}��J�@��*��ʍ�4hB3m��t���?bm�]!��t�I�L*�Rܹ�-�|čwDh�$p�w�͝A�p�P�!t�y$�3<K����QbV�B0o�A���|�}�\�F�$Q��R��τ��_��#�+�V>����v��+-�o����3􎕆��.m��Ӈdj�Z^�9��mZ=]:�*��1�v���P���Q��ʔ�:�4M�lbl�`D�z��lN4��Z���R�r;Y�_�u��Y����c�೘��A��B!d����n�S+��u�eu;n۲[�x�"&&讱��\b6l�Cp����އ���ҌE��� xzdOQ���: _ZފC    8�    �  �   G  U   O������	e���5zY�L�DRaB            x��T�J�@^��+��-��&Q�Ջ�ZQ���-�)��&]���ͶD_Ń���/��'�5<:i�)��B&;��|��-=!���(�p�	F5nU���A�S�H�Zq�`���k�����0D���_��Ad֭�B�ۻp:�?]�-�@k�M`@g��:'ܓ�Wj�\�t3M(2���i�q��M��0�P���ڹ��P1;7�Ӿ�i��F�!%�撊����;��P���?#�{ȷ���a��\:��>T]`�+`�|@\i>���"�B���$Ab8@�9��cE�!�_��MNK�DDg�=�ͲV����_���� "�.�k���3���J;�XM1�R;�k�uS1wH�)�'v݂�{ж�������f��U�a�{�*��0�˙:s�����Һ�g$
�2�c7    :�    �  
   G  g   O�������3��a�&��b-E|w�'            x��T�j�@޶"^�?��P(,\�;b��R�ў(��B�R{�&Y�����q�/��D�)��O�o��4d23��|��e���/��|#�<�v�P"�t�y4.�J*a��N��=�<�NN��H�y�S���q��vi� ~Q���H��9�|�q S->����K�Njk��\�C@���A$S:�]��3:����e���|�'!���9��DoG�!���`��i�D�����es{�?�S&u�f�X"���E��gB�~t��������q4�0�wƝ�_p�)9�	���K{pJ}�x��SS�k(��k�kë\�W�+��h�m\���m�O�m�QY��W�§��<�U@j�`�k��[�-=�=�Ma��bw0��Iέ���-X`�n�Ȃ�ϋ�A�J]�uI�/�f�h9KKc�S�I}�o'r;�v��8�ơp)M��p]�D�8�7![�/�.��ߐw3]I�y<