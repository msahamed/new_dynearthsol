# plot
import matplotlib.pyplot as plt
from matplotlib import rcParams
import pickle
import numpy as np


pickle_file_name = 'energy_data_no_starin_weakening.pickle'
# pickle_file_name = 'energy_data_low_800_resolution.pickle'

file = open(pickle_file_name, 'rb')
data = pickle.load(file, encoding='latin1')
avg_data_t = data['avg_data_t']
max_data_t = data['max_data_t']
avg_data_e = data['avg_data_e']
max_data_e = data['max_data_e']

# FZT = Energy, zero temperature 
# FFT = Fixed, 10 degree dilation angle
# FRT = Reduced with plastic strain
# FRD = Deviatoric

models = dict(FZT = 0, 
            #   FFT = 1,
              FRT = 2,
              FRD = 3)

field_name = ["strain-rate II log10", "energy_elastic", "energy_thermal"]
num_of_models = len(models)
number_of_files = 400


fig = plt.figure(figsize=(11,6))
font_size = 16
plt.rcParams.update({'font.size': font_size})
extensions = np.linspace(0, 40, number_of_files)
# rcParams['axes.titlepad'] = 20
plt.rc('text', usetex=True)
plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.35)

indx = list(models.values())
model_names = list(models.keys())

for i in range(num_of_models):
    # elastic energy
    plt.subplot(1,3,1)
    plt.plot(extensions, np.absolute(avg_data_e[indx[i], :]/1e6), label=str(model_names[i]+'$^{\prime}$'), linewidth=3)
    plt.title(r'\textbf{(a)} Elastic energy density', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'$\vert$Elastic energy density ($MJ/m^3$)$\vert$', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)
    plt.legend(loc=2)

    # thermal energy
    plt.subplot(1,3,2)
    plt.plot(extensions, np.absolute(avg_data_t[indx[i], :]/1e6), linewidth=3)
    plt.title(r'$\textbf{(b)}$ Thermal energy density', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'$\vert$Thermal energy density ($MJ/m^3$)$\vert$', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

    # Total energy
    plt.subplot(1,3,3)
    plt.plot(extensions, np.absolute((avg_data_e[indx[i], :] + avg_data_t[indx[i], :])/1e6), linewidth=3)
    plt.title(r'$\textbf{(c)}$ Total energy density', loc='left', y=1.05)
    plt.xlabel(r'Extension (km)', size=font_size)
    plt.ylabel(r'$\vert$Total energy density ($MJ/m^3$)$\vert$', size=font_size)
    plt.xticks([0, 10, 20, 30, 40], fontsize = font_size)

plt.tight_layout()
plt.show()