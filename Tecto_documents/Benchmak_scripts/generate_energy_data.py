
from paraview.simple import *
import numpy as np
import pickle


# Models name
model_name = ['energy', 'fixed', 'variable', 'deviatoric']
# model_name = ['diffusion_800_new', 'energy_800_new', 'fixed_800_new', 'variable_800_new', 'deviatoric_800_new']
models = ['FZT', 'FFT', 'FRT', 'FRD']
field_name = ["energy_volumetric", "energy_deviatoric", "energy_total_vol_dev"]
num_of_models = len(models)
number_of_files = 400

# Directory path
path = "/Volumes/Mini/energy_balance_no_weakening/combined/"
# path = "/Volumes/Mini/energy_balance_weakening/combined/low_800_resolution/"
# path = "/Volumes/Mini/energy_balance_weakening/combined/high_400_resolution/"

# Pickle file name:
pickle_file_name = 'power_no_strain_weakening.pickle'

# Initialize array
# max_pstrain = np.zeros((num_of_models, number_of_files))
# avg_pstrain = np.zeros((num_of_models, number_of_files))
# avg_data_t = np.zeros((num_of_models, number_of_files))
# avg_data_e = np.zeros((num_of_models, number_of_files))

avg_vol = np.zeros((num_of_models, number_of_files))
avg_dev = np.zeros((num_of_models, number_of_files))
avg_total = np.zeros((num_of_models, number_of_files))

# max_data_t = np.zeros((num_of_models, number_of_files))
# max_data_e = np.zeros((num_of_models, number_of_files))
# max_pstrain = np.zeros((num_of_models, number_of_files))

max_vol = np.zeros((num_of_models, number_of_files))
max_dev = np.zeros((num_of_models, number_of_files))
max_total = np.zeros((num_of_models, number_of_files))


for i in range(num_of_models):
    for j in range(number_of_files):
        file_name = str(model_name[i])+ '.'
        File_name = path + file_name + str(format(j, '06d')) + ".vtu"
        File = XMLUnstructuredGridReader(FileName=[File_name])
        DataSliceFile = paraview.servermanager.Fetch(File)
        new  = DataSliceFile.GetCellData()

        #------- Area calculation ------
        integrateVariables1 = IntegrateVariables(Input=File)
        DataSliceFile = paraview.servermanager.Fetch(integrateVariables1)
        cell_data  = DataSliceFile.GetCellData()
        area_arry = cell_data.GetArray('Area')
        area = np.array(area_arry)
        
        vol = new.GetArray(field_name[0])
        dev = new.GetArray(field_name[1])
        total = new.GetArray(field_name[2])

        avg_vol[i][j] = np.sum(vol)/area[0]
        avg_dev[i][j] = np.sum(dev)/area[0]
        avg_total[i][j] = np.sum(total)/area[0]

        max_vol[i][j] = np.amax(vol)
        max_dev[i][j] = np.amax(dev)
        max_total[i][j] = np.amax(total)
    
data = {'max_vol':max_vol, 'max_dev':max_dev, 'max_total':max_total, 'avg_vol':avg_vol, 'avg_dev':avg_dev, 'avg_total':avg_total} 

file = open(pickle_file_name, 'wb')
pickle.dump(data, file)
file.close()

print("Created pickle file")
