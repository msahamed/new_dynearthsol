#!/bin/sh
#PBS -l nodes=1:default:ppn=1
#PBS -l walltime=600:00:00
#PBS -A [CERI]
#PBS -N energy

#export OMP_NUM_THREADS=1

# source the module command

source /etc/profile.d/modules.sh

cd /home/msahamed/energy_balance_new/dynearthsol3d_energy/result

 ./../dynearthsol2d ./core_complex_energy.cfg
